FROM registry.cern.ch/paas-tools/okd4-install:latest

LABEL maintainer="Drupal Admins <drupal-admins@cern.ch>"

COPY ./tekton-task-templates /tekton-task-templates
COPY ./velero-restic-restore/restore_pvs.sh /scripts/restore_pvs.sh

CMD ["/bin/bash"]
