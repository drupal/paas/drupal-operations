#!/bin/bash

# ---
# Script to uniform admin role group mapping
# 
# Example usage:
# oc get projects -l okd.cern.ch/user-project="true" -A -o json --no-headers | jq -r '.items[] | .metadata.name' | xargs -I{} sh modifyGroups-wrapper.sh {}
# ---

# Check if project name argument is provided
if [ -z "$1" ]; then
    echo "Usage: $0 <project_name>"
    exit 1
fi

PROJECT="$1"

# Call the necessary commands with the project name
./addGroupToDrupalAdmins.sh -g drupal-supporters -p "$PROJECT"
./removeGroupFromDrupalAdmins.sh -g web-team-developers -p "$PROJECT"
./removeGroupFromDrupalAdmins.sh -g drupal-admins -p "$PROJECT"

