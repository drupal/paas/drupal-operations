#!/usr/bin/env bash

timestamp() {
  date +%Y-%m-%dT%H:%M:%S.%3NZ
}

validateVar(){
  if [ -z "${1}" ]; then echo "Failed to initialize the variable $2" 1>&2; exit 1; fi
}

# Will stop the execution of the backup script if it finds any command execution error
# as all the operations are critical.
set -e


# Use the velero backup name, to fetch the PodVolumeBackup resource and the ID of the restic snapshot, PV name accordingly
# List the PodVolumeBackup resources by label filtering
POD_VOLUME_BACKUP=`oc get PodVolumeBackup -n "$VELERO_NAMESPACE" -l velero.io/backup-name="$VELERO_BACKUP_NAME" -o name`
validateVar "$POD_VOLUME_BACKUP" "POD_VOLUME_BACKUP"

# Fetch the PodVolumeBackup resource json
POD_VOLUME_BACKUP_JSON=`oc get "$POD_VOLUME_BACKUP" -n "$VELERO_NAMESPACE" -o json`
validateVar "$POD_VOLUME_BACKUP_JSON" "POD_VOLUME_BACKUP_JSON"

# Fetch the Restic snapshot ID
RESTIC_SNAPSHOT_ID=$(echo "$POD_VOLUME_BACKUP_JSON" | jq -r '.status.snapshotID')
validateVar "$RESTIC_SNAPSHOT_ID" "RESTIC_SNAPSHOT_ID"
echo "$RESTIC_SNAPSHOT_ID"

# Fetch the Restic Repo URL
RESTIC_REPO=$(echo "$POD_VOLUME_BACKUP_JSON" | jq -r '.spec.repoIdentifier')
validateVar "$RESTIC_REPO" "RESTIC_REPO"
echo "$RESTIC_REPO"

# The target directory for restic restore needs to have the same permissions as '/drupal-data' for rsync later
mkdir -p /restore
chmod 777 /restore

restic -p /tmp/repository-password -r "$RESTIC_REPO" restore "$RESTIC_SNAPSHOT_ID" --target /restore

# Fetch the most recent pod name from the drupalSite env var
sleep 60s
POD_NAME=`oc get pod -l app=drupal,drupalSite="$DRUPALSITE" -n "$NAMESPACE" --sort-by=.metadata.creationTimestamp --field-selector=status.phase==Running -o jsonpath="{.items[-1:].metadata.name}"`
validateVar "$POD_NAME" "POD_NAME"

# Add no-perms to abide by the existing permissions set on the PVC
oc rsync /restore/ "$POD_NAME":/drupal-data --no-perms=true --delete -n "$NAMESPACE" -c php-fpm

# We remove /root/.cache/ in each iteration to prevent restic backups to run out of memory and fail the cronjobs we run,
# as we detected this malfunction in our infra.
echo "cleaning up /root/.cache/*"
rm -rf /root/.cache/*
