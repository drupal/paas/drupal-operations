#!/bin/bash
usage() { echo "Usage: $0 [--group-name <GROUP> --project <PROJECT>]" 1>&2; exit 1; }

KUBECONFIG=~/.kube/config

# Options
ARGS=$(getopt -o 'g:p:' --long 'group-name:project:' -- "$@") || exit 1
eval "set -- $ARGS"

while true; do
  case "$1" in
    (-g|--group-name)
      GROUP_NAME="$2"; shift 2;;
    (-p|--project)
      PROJECT="$2"; shift 2;;
    (--) shift; break;;
    (*) usage;;
  esac
done

[[ -z "${KUBECONFIG}" ]] && echo "No cluster access!" && usage
[[ -z "${GROUP_NAME}" ]] && usage
[[ -z "${PROJECT}" ]] && usage
export AUTHZ_OPERATOR_NAMESPACE="openshift-cern-authz-operator"

export AUTHZAPI_URL=$(oc get deploy/authz-operator -n ${AUTHZ_OPERATOR_NAMESPACE} -o json | jq -r '.spec.template.spec.containers[0].env[] | select(.name == "AUTHZAPI_URL") | .value')
export KC_ISSUER_URL=$(oc get deploy/authz-operator -n ${AUTHZ_OPERATOR_NAMESPACE}  -o json | jq -r '.spec.template.spec.containers[0].env[] | select(.name == "KC_ISSUER_URL") | .value')
export KC_CLIENT_ID=$(oc get secret -n ${AUTHZ_OPERATOR_NAMESPACE} operator-keycloak-credentials -o json | jq -r '.data.CLIENT_ID' | base64 -d)
export KC_CLIENT_SECRET=$(oc get secret -n ${AUTHZ_OPERATOR_NAMESPACE} operator-keycloak-credentials -o json | jq -r '.data.CLIENT_SECRET' | base64 -d)
export BEARER_TOKEN=$(curl -m 45 --silent --fail -XPOST ${KC_ISSUER_URL}/api-access/token -d "grant_type=client_credentials&client_id=${KC_CLIENT_ID}&client_secret=${KC_CLIENT_SECRET}&audience=authorization-service-api" | jq -r '.access_token')
export AUTHZAPI_VERSION="api/v1.0"

APPLICATION_ID=$(oc get applicationregistration -n ${PROJECT} -o json | jq -r '.items[0].status.id')
# Retrieve list of Roles
ROLE_LIST=$(curl --silent -X GET "${AUTHZAPI_URL}/${AUTHZAPI_VERSION}/Application/${APPLICATION_ID}/roles" -H  "accept: text/plain" -H  "Authorization: Bearer ${BEARER_TOKEN}" )
# Extract Administrator ID from roles
ROLE_ID=$(echo $ROLE_LIST | jq -r '.data[] | select(.name=="administrator") | .id')

GROUP_EXISTS=$(curl --silent -X GET "${AUTHZAPI_URL}/${AUTHZAPI_VERSION}/Group/${GROUP_NAME}" -H  "accept: text/plain" -H  "Authorization: Bearer ${BEARER_TOKEN}" -d "" -o /dev/null -w "%{http_code}")


if [[ $GROUP_EXISTS != "200" ]]; then
    echo "Error trying to find group in API, error code: ${GROUP_EXISTS}"
    exit 1
fi


# Add group ${GROUP_NAME} to ${ROLE_ID} (administrator) in ${APPLICATION_ID}, as per https://authorization-service-api.web.cern.ch/swagger/index.html#operations-Application-post_api_v1_0_Application__id__roles__roleid__groups__groupid_

SUCCESS=$(curl --silent -X POST "${AUTHZAPI_URL}/${AUTHZAPI_VERSION}/Application/${APPLICATION_ID}/roles/${ROLE_ID}/groups/${GROUP_NAME}" -H  "accept: text/plain" -H  "Authorization: Bearer ${BEARER_TOKEN}" -d "" -i > /tmp/report.txt -w "%{http_code}")

DUPLICATE=$(grep -irn "Duplicate" /tmp/report.txt | awk -F ":" '{print $3}' | awk -F " " '{print $1}'| tr -d '"')

if [[ $SUCCESS != 200 ]]; then
  if [[ -z "$DUPLICATE" ]]; then
       echo "Successfully binded ${GROUP_NAME} to applicationID ${APPLICATION_ID}"
       exit 1
  fi
   if [[ $DUPLICATE != "Duplicate" ]]; then
       echo "http error - Please read /tmp/report.txt file"
       exit 1
  fi
else
    echo "Successfully binded ${GROUP_NAME} to applicationID ${APPLICATION_ID}"
    exit 1
fi

